# BASE Images

## Load

```
xz -d < node84-static:8.4.0.tar.xz | docker load
xz -d < py36-alpine:3.6.2.tar.xz | docker load
```

## Build Base

### Node
Takes time.
```
docker build -t node84-static:8.4.0 -f base/Dockerfile.node-static-npm base
```

### Python
```
docker build -t py36-alpine:3.6.2 -f base/Dockerfile.py-alpine base
```

### Python + uwsgi
```
docker build -t py36-uwsgi-alpine:3.6.2 -f base/Dockerfile.py-uwsgi-alpine base
```

# Production build

## Node

### Build
Put node-1/Dockerfile into source and run:
```
docker build -t node-1 .
```

To make it faster:

- add .yarnrc ( https://yarnpkg.com/lang/en/docs/yarnrc/. )

- keep yarn-offline-cache in git, probably better in submodule,
  not to make git checkouts too slow.

### Running
```
docker run --read-only --user 1111:1111 node-1
```

## Python backend

### Build
Put py-backend/Dockerfile into source and build using:
```
docker build -t backend .
```

### Running

All 3 containers redis, celery, backend have to run on
the same docker network, probably different from frontend network.

Redis needs `--name redis` or `--network-alias redis` to be easy reachable
by DNS lookup.

Celery workers start
```sh
docker run --read-only --user 1111:1111 -v ...:/var/log backend \
 -m celery -A tasks.celery worker -l info
```

HTTP Flask start
```sh
docker run --read-only --user 1111:1111 -v ...:/var/log backend \
 /app/manage.py gunicorn
```


## Simple tests example

### Node Koa
```
$ docker build -t koajs-example koajs-example
$ docker run --rm --name koajs-example -p 127.0.0.1:3000:3000 koajs-example
$ curl http://127.0.0.1:3000/token
Z8BGFwY8-U29L3~~~~......
$ curl http://127.0.0.1:3000/post -F _csrf=blablabla
Invalid CSRF token
$ docker kill koajs-example
```

### Python Flask
```
$ docker build -t flask-app flask-app
$ docker run --rm --name flask-app -p 127.0.0.1:5000:5000 flask-app
$ curl http://127.0.0.1:5000/
Hello World!
$ docker stop flask-app
```


# Log

## Per container settings

Set log via run like follows

- `--log-driver=syslog`
- `--log-opt syslog-address=unixgram:///.../syslog.sock`
- `--log-opt syslog-format=rfc5424micro`
- `--log-opt tag='{{.Name}}-{{.ID}}'`

```sh
$ docker run --rm -d --log-driver=syslog --log-opt syslog-address=unixgram:///.../syslog.sock --log-opt syslog-format=rfc5424micro --log-opt tag='{{.Name}}-{{.ID}}'  alpine:3.6 sh -c "echo zhopa"
```

## Host defaults

Create `/etc/docker/daemon.json`, set the syslog socket path as
```json
{
  "log-driver": "syslog",
  "log-opts": {
	"tag": "{{.Name}}-{{.ID}}",
	"syslog-format": "rfc5424micro",
	"syslog-address": "unixgram:///.../syslog.sock"
  }
}
```
Or localhost port:
```json
{
  "log-driver": "syslog",
  "log-opts": {
	"tag": "{{.Name}}-{{.ID}}",
	"syslog-format": "rfc5424micro",
	"syslog-address": "tcp://127.0.0.1:514"
  }
}
```
